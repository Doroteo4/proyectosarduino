const int sensorPin= A0;
 
void setup()
{
  Serial.begin(9600);
}
 
void loop()
{
  int value = analogRead(sensorPin);
  float millivolts = (value / 1023.0) * 5000;
  float celsius = millivolts / 10; 
  Serial.print(celsius);
  Serial.println(" C");
  delay(1000);
}
  

//int val;
//int tempPin = 1;
//
//void setup()
//{
//  Serial.begin(9600);
//}
//void loop()
//{
//  val = analogRead(tempPin);
//  float mv = ( val/1024.0)*5000;
//  float cel = mv/10;
//  float farh = (cel*9)/5 + 32;
//  Serial.print("TEMPRATURE = ");
//  Serial.print(cel);
//  Serial.print("*C");
//  Serial.println();
//  delay(1000);
///* uncomment this to get temperature in farenhite
//Serial.print("TEMPRATURE = ");
//Serial.print(farh);
//Serial.print("*F");
//Serial.println();
//*/
//}
