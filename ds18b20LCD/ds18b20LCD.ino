//[/code]
/*
Aray Innovations LLC
Sketch realizado por Virgilio Enrique Aray Arteaga
Enero del 2018
V 0.1
*/
//
// L I B R E R I A S
//
//Librerias necesarias para controlar pantalla LCD
//Libraries needed to control LCD screen
//
#include <Wire.h>
#include <LiquidCrystal_I2C.h>  // Para la pantalla 16x2
//
//Librerias necesarias para controlar el sensor de Temperatura DS18B20
//Libraries needed to control the Temperature sensor DS18B20
#include <OneWire.h>
#include <DallasTemperature.h>
//
// VARIABLES Y CONSTANTES
//
// variables para mostrar mensaje de inicio en la pantalla LCD
// variables to display power on message on the LCD screen
//
int contador=0;   // para el ciclo for / for the "for" cycle
int fil = 0;      // fila / row
int col = 0;      // columna / column
//
// arreglo para formatear el mensaje incial al necender el dispositivo
// array to format the initial message when turning on the device
//
// textos a mostrar / texts to show
// 
//   0123456789012345
//
// 0 Welcome, this is
// 1 a Digital       
//
//   0123456789012345
// 
// 0 Thermometer     
// 1 Version  0.1    
//
//   0123456789012345
//
// 0 Aray Innovations
// 1 January 2018 
//
const char Mensaje01[] = "Welcome, this isa Digital       Thermometer     Version  0.1    Aray InnovationsJanuary 2018    *";
//                        01234567890123450123456789012345012345678901234501234567890123450123456789012345
//                        0         1     0         1     0         1     0         1             
//---
//
// Pin digital del arduino que recibirá el dato desde el sensor DS18B20
// Digital pin of the arduino that will receive the data from the sensor DS18B20
//
const int pinDatosDQ = 7; 
//
//-- Variables para almacenar temperaturas
//
float TempAnterior=0.00;  // almacena temperatura previa / stores previous temperature
float TempActualCB=0.00;  // temp Centigrados
float TempActualFB=0.00;  // temp Fahrenheit
//
// -- Instancia Pantalla LCD / Instance LCD Screen
// 
// Para iniciar el objeto se necesita dar la direccion hex, el numero de columnas y filas
// To start the object you need to give the hex address, the number of columns and rows
//
// la direccion 0X3F puede ser diferente en otra pantalla
// the address 0X3F may be different on another screen
//
LiquidCrystal_I2C lcd(0X3F,16,2);  // Variable LCD pantalla de 16 x 2
//
//
// -- Instancia a las clases OneWire y DallasTemperature --
// -- Instance to the OneWire and DallasTemperature classes
//
OneWire oneWireObjeto(pinDatosDQ);
DallasTemperature sensorDS18B20(&oneWireObjeto);
//
//--  ---
//                  
void setup()
{

  //
  //  LCD Init
  //
  lcd.init();
  lcd.clear();      // Limpiamos la LCD / clear the LCD
  lcd.backlight();  // luz de fondo on / backlight on
  //
  // -- Mensaje de Bienvenida / Welcome message ---  
  //
  fil=0;
  col=0;
  for (contador=0;contador<200;contador++){
      lcd.setCursor(col,fil);
      lcd.print(Mensaje01[contador]);
      delay(25);      // espera 25 milisegundos entre letras / wait 25 miliseconds between chars
      col++;
      if (col==16) {
        col=0;
        fil++;
        if (fil>1) {
          fil=0;
          delay(1000); // espera 1 seg entre mensajes / wait 1 sec between mesages
          lcd.clear();
        }
      }
      if (Mensaje01[(contador+1)]=='*') contador=200; // * indica fin de mensaje / "*" indicates end of message
  }
  lcd.clear(); // Limpiamos la LCD / clear the LCD
  //
  //  Por alguna razón la primera lectura es incorrecta
  //  For some reason the first reading is incorrect
  //
  sensorDS18B20.requestTemperatures();
  TempAnterior = sensorDS18B20.getTempCByIndex(0);
  //
  //
  //
}
//
//
//
void loop()
{
  //
  //  Se lee y se muestra la temperatura en ºC y ºF
  //  The temperature is read and displayed in ° C and ° F
  //
  sensorDS18B20.requestTemperatures();
  TempActualCB = sensorDS18B20.getTempCByIndex(0);  // Temp Celcius
  TempActualFB = sensorDS18B20.getTempFByIndex(0);  // Temp Fahrenheit
  //
  // Si la temperatura ha cambiado o es la primera lectura se la muestra en el LCD
  // If the temperature has changed or is the first reading it is displayed on the LCD
  // 
  if (TempActualCB != TempAnterior) {
     TempAnterior = TempActualCB;
     //
     lcd.setCursor(0,0);  // columna 0, fila 0 / column 0, row 0
     lcd.print("Temp C ");
     lcd.print(TempActualCB);
     lcd.setCursor(0,1);  // columna 0, fila 1 / column 0, row 1
     lcd.print("Temp F ");
     lcd.print(TempActualFB);
     //
     delay(1000);  // espera 1 segundo / wait 1 second
  }
}
//
// end of sketch
//

//[/code]
